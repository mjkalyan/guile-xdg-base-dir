(define-module (xdg-base-dir)
  #:export (xdg-cache-home
            xdg-config-home
            xdg-data-home
            xdg-state-home
            xdg-runtime-dir
            xdg-config-dirs
            xdg-data-dirs))


;; The idea is to be able to run this:
;; (map define-xdg-base-dir
;;      '("XDG_CACHE_HOME" "XDG_CONFIG_HOME" "XDG_DATA_HOME" "XDG_STATE_HOME")
;;      '(".cache"         ".config"         ".local/share"  ".local/state"))
;; using this macro:
;; (use-modules (ice-9 string-fun))
;; (define-syntax-rule (define-xdg-base-dir name default-path)
;;   `(define (,(string->symbol (string-downcase (string-replace-substring name "_" "-"))))
;;      (let ((path (getenv ,name)))
;;        (if path
;;            path
;;            (canonicalize-path (string-append (getenv "HOME") "/" ,default-path))))))


;; User directories
(define (xdg-cache-home)
  (let ((path (getenv "XDG_CACHE_HOME")))
    (or path (string-append (getenv "HOME") "/.cache"))))
(define (xdg-config-home)
  (let ((path (getenv "XDG_CONFIG_HOME")))
    (or path (string-append (getenv "HOME") "/.config"))))
(define (xdg-data-home)
  (let ((path (getenv "XDG_DATA_HOME")))
    (or path (string-append (getenv "HOME") "/.local/share"))))
(define (xdg-state-home)
  (let ((path (getenv "XDG_STATE_HOME")))
    (or path (string-append (getenv "HOME") "/.local/state"))))
(define (xdg-runtime-dir)
  (let ((path (getenv "XDG_RUNTIME_DIR")))
    (or path
        ;; This is the elogind/systemd default, not sure if it'll break on other systems
        (string-append  "/run/user/" (getenv "UID")))))


;; System directories
(define (xdg-config-dirs)
  (let ((path (getenv "XDG_CONFIG_DIRS")))
    (or path "/usr/local/share:/usr/share")))
(define (xdg-data-dirs)
  (let ((path (getenv "XDG_DATA_DIRS")))
    (or path "/etc/xdg")))
